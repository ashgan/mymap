$(function () {
    var map_center = [6336, -13504]
    var browserLanguage = navigator.language.substring(0, 2).toLowerCase(); // userLanguage pour compatibilité avec certains vieux navigateurs
    console.log("Langue du navigateur :", browserLanguage);

    var supportedLanguages = ['en', 'fr', 'de', 'es', 'ru'];

    if (supportedLanguages.includes(browserLanguage)) {
        console.log("Langue supportée :", browserLanguage);
        var userLanguage = browserLanguage
    } else {
        console.log("Langue non supportée, valeur par défaut appliquée.");
        var userLanguage = 'en'
    }

    var map = Ryzom.map('map', {rzMode: 'world',
                                rzLang: userLanguage,
                                fullscreenControl: true,
                                fullscreenControlOptions: {
                                    position: 'topleft',
                                    keyboard: true
                                },
                                preferCanvas: true,
                                renderer: L.canvas(),
                            });
    map.setView(map_center, 6);
    $('.leaflet-control-attribution').hide()

    var translations = {
        "Faction": {
            "fr": "Faction",
            "es": "Facción",
            "en": "Faction",
            "de": "Fraktion",
            "ru": "Фракция"
        },
        "Wiki Link": {
            "fr": "Lien Wiki",
            "es": "Enlace Wiki",
            "en": "Wiki Link",
            "de": "Wiki-Link",
            "ru": "Ссылка на вики"
        },
        "Wiki Not Available": {
            "fr": "Lien Wiki non disponible",
            "es": "Enlace Wiki no disponible",
            "en": "Wiki link not available",
            "de": "Wiki-Link nicht verfügbar",
            "ru": "Ссылка на вики недоступна"
        },
        "Affilié": {
        "fr": "Affilié",
        "es": "Afiliado",
        "en": "Affiliated",
        "de": "Zugehörig",
        "ru": "Принадлежащий"
        }
    };
//    L.easyButton('<i class="iconify" data-icon="fluent:weather-hail-day-20-filled"></i>', function(btn, map){
//        window.open('https://ryzom.eu.org', '_blank');
//    }, "Météo d'Atys").addTo(map);

    var customControl;
    // Fonction pour mettre à jour le contrôle personnalisé
    function updateSeasonControl() {
        // Vérifier si la case à cocher est cochée
        var checkbox = document.getElementById('nextseason');
        if (!checkbox.checked) {
            // Si la case n'est pas cochée, retirer le contrôle personnalisé s'il existe
            if (customControl) {
                customControl.remove();
                customControl = null;  // Remettre à null après avoir retiré le contrôle
            }
            return;  // Ne pas effectuer la requête AJAX
        }

        // La case est cochée, effectuer la requête AJAX
        $.getJSON("/next_season", function(controlContent) {
            var customControlContent = '<div id="customControl">' + controlContent.message;
            customControlContent += '</div>';

            // Retirer le contrôle personnalisé s'il existe déjà
            if (customControl) {
                customControl.remove();
            }

            // Créer le contrôle personnalisé
            customControl = L.control({position: 'bottomright'});
            customControl.onAdd = function(map) {
                var div = L.DomUtil.create('div', 'leaflet-bar');
                div.innerHTML = customControlContent;
                return div;
            };

            customControl.addTo(map);
        });
    }

    // Appeler la fonction initiale
    updateSeasonControl();

    // Mettre à jour automatiquement toutes les 30 minutes
    var intervalId = setInterval(function() {
        updateSeasonControl();
    }, 30 * 60 * 1000); // 30 minutes en millisecondes

    const sidepanelRight = L.control.sidepanel('mySidepanelRight', {
        panelPosition: 'right',
        tabsPosition: 'top',
        pushControls: true,
        darkMode: true,
        startTab: 'tab-1'
    }).addTo(map);

    var iconMapping = {
        best: L.icon({ iconUrl: 'static/images/best.png', iconSize: [32, 32], iconAnchor: [16, 16] }),
        good: L.icon({ iconUrl: 'static/images/good.png', iconSize: [32, 32], iconAnchor: [16, 16] }),
        bad: L.icon({ iconUrl: 'static/images/bad.png', iconSize: [32, 32], iconAnchor: [16, 16] }),
        worst: L.icon({ iconUrl: 'static/images/worst.png', iconSize: [32, 32], iconAnchor: [16, 16] }),
    };

    var WeathercontinentPositions = {
        fyros: {lat: 17976, lng: -24820},
        matis: {lat: 3712, lng: -3024},
        tryker: {lat: 17948, lng: -30560},
        zorai: {lat: 9572, lng: -2760},
        nexus: {lat: 8876, lng: -6936},
        newbieland: {lat: 9200, lng: -10824},
        kitiniere: {lat: 2248, lng: -16616},
        matis_island: {lat: 14684, lng: -688},
        route_gouffre: {lat: 6288, lng: -12640},
        terre: {lat: 1676, lng: -14080},
        bagne: {lat: 936, lng: -10228},
        sources: {lat: 3196, lng: -10248},
    };

    // Créer un contrôle de champ de recherche animé
    var AnimatedSearchBox = L.Control.extend({
        onAdd: function(map) {
            var container = L.DomUtil.create('div', 'animated-search-box');

            var form = L.DomUtil.create('form', '', container);
            form.id = 'searchbar';
            form.acceptCharset = 'UTF-8';

            var input = L.DomUtil.create('input', 'search-input', form);
            input.type = 'search';
            input.id = 'search';
            input.placeholder = 'Rechercher...';

            L.DomEvent.addListener(input, 'keydown', function(event) {
                // Arrêter la propagation de l'événement de la touche
                L.DomEvent.stopPropagation(event);
            });

            var searchIcon = L.DomUtil.create('div', 'search-icon', container);
            searchIcon.innerHTML = '<img src="static/images/search_icon.png">';

            L.DomEvent.disableClickPropagation(container);

            L.DomEvent.addListener(searchIcon, 'click', function() {
                input.classList.toggle('active');
            });

            return container;
        }
    });

    // Ajouter le contrôle de champ de recherche à la carte
    new AnimatedSearchBox({ position: 'topright' }).addTo(map);

    // récupérer le JSON contenant les coordonnées des polylines
    $.getJSON('/vortex_path', { lang: userLanguage }, function(data) {
        // parcourir chaque objet dans le tableau de données
        $.each(data, function(index, obj) {
            var polyline = L.polyline(obj.coords, obj.style);
            if (obj.popup) {
            polyline.bindPopup(obj.popup);
            }

            if (obj.layergroup === 'path') {
                polyline.addTo(layerGroups.pathLayerGroup);
            }
            else {
                polyline.addTo(layerGroups.VortexLayerGroup);
            }
        });


    });

    // afficher chaque polyline si le niveau de zoom est inférieur ou égal à 6
    map.on('zoomend', function() {
        var zoom = map.getZoom();
        if (zoom <= 6) {
            layerGroups.VortexLayerGroup.eachLayer(function(layer) {
                if (layer instanceof L.Polyline) {
                    layer.setStyle({opacity: 1});
                }
            });
            document.getElementById("WeatherLayerGroup").checked = true;
            updateLayers()
        } else {
            layerGroups.VortexLayerGroup.eachLayer(function(layer) {
                if (layer instanceof L.Polyline) {
                    layer.setStyle({opacity: 0});
                    }
            });
            document.getElementById("WeatherLayerGroup").checked = false;
            updateLayers()
        }
    });

    // Tableau pour stocker les markers
    var markers = [];
    const layerGroups = {tpLayerGroup: L.layerGroup(),
                         VortexLayerGroup: L.layerGroup(),
                         WeatherLayerGroup: L.layerGroup(),
                         pathLayerGroup: L.layerGroup(),
                         tribeLayerGroup: L.layerGroup(),
                         kamitribeLayerGroup: L.layerGroup(),
                         karatribeLayerGroup: L.layerGroup(),
                         marautribeLayerGroup: L.layerGroup(),
                         affiliateLayerGroup: L.layerGroup(),
                         banditLayerGroup: L.layerGroup(),
                         outpostLayerGroup: L.layerGroup(),
                         GuildLayerGroup: L.layerGroup(),
                         BossLayerGroup: L.layerGroup(),
                         MarauderBossLayerGroup: L.layerGroup(),
                         NhLayerGroup: L.layerGroup(),
                         NpcLayerGroup: L.layerGroup(),
                         TempleLayerGroup: L.layerGroup(),
                         worstTrykerLayerGroup: L.layerGroup(),
                         badTrykerLayerGroup: L.layerGroup(),
                         goodTrykerLayerGroup: L.layerGroup(),
                         bestTrykerLayerGroup: L.layerGroup(),
                         worstMatisLayerGroup: L.layerGroup(),
                         badMatisLayerGroup: L.layerGroup(),
                         goodMatisLayerGroup: L.layerGroup(),
                         bestMatisLayerGroup: L.layerGroup(),
                         worstZoraiLayerGroup: L.layerGroup(),
                         badZoraiLayerGroup: L.layerGroup(),
                         goodZoraiLayerGroup: L.layerGroup(),
                         bestZoraiLayerGroup: L.layerGroup(),
                         worstFyrosLayerGroup: L.layerGroup(),
                         badFyrosLayerGroup: L.layerGroup(),
                         goodFyrosLayerGroup: L.layerGroup(),
                         bestFyrosLayerGroup: L.layerGroup()
                         };

    // Fonction pour activer ou désactiver les layersGroups
//    function updateLayers() {
//        const checkboxes = document.querySelectorAll(".layer-group");
//        const tribeCheckbox = document.getElementById("tribeLayerGroup");
//
//        checkboxes.forEach(checkbox => {
//            const layerGroup = layerGroups[checkbox.id];
//            if (checkbox.checked) {
//                map.addLayer(layerGroup);
//            } else {
//                map.removeLayer(layerGroup);
//            }
//        });
//    }

//document.addEventListener("DOMContentLoaded", function () {
    const checkboxes = document.querySelectorAll(".layer-group");
    const filtersDiv = document.getElementById("filters");
    const tribeCheckbox = document.getElementById("tribeCheckbox");
    const factionRadios = document.querySelectorAll('input[name="factionFilter"]');
    const allRadio = document.getElementById("tribeLayerGroup");

    function updateLayers() {
        checkboxes.forEach(checkbox => {
            const layerGroup = layerGroups[checkbox.id];
            if (layerGroup) {
                if (checkbox.checked) {
                    map.addLayer(layerGroup);
                } else {
                    map.removeLayer(layerGroup);
                }
            }
        });

        if (tribeCheckbox.checked) {
            filtersDiv.style.display = "block";
            if (!Array.from(factionRadios).some(radio => radio.checked)) {
                allRadio.checked = true;
            }
            updateFactionLayers();
        } else {
            filtersDiv.style.display = "none";
            map.removeLayer(layerGroups["tribeLayerGroup"]);
            Object.keys(layerGroups).forEach(layer => {
                if (layer.includes("tribeLayerGroup")) {
                    map.removeLayer(layerGroups[layer]);
                }
            });
        }
    }


    function updateFactionLayers() {
        // Désactiver toutes les factions
        map.removeLayer(layerGroups["kamitribeLayerGroup"]);
        map.removeLayer(layerGroups["karatribeLayerGroup"]);
        map.removeLayer(layerGroups["marautribeLayerGroup"]);
        map.removeLayer(layerGroups["tribeLayerGroup"]);

        // Activer celle sélectionnée
        const selectedFaction = document.querySelector('input[name="factionFilter"]:checked');
        if (selectedFaction) {
            if (selectedFaction.id === "tribeLayerGroup") {
                map.addLayer(layerGroups["tribeLayerGroup"]);
            } else {
                map.addLayer(layerGroups[selectedFaction.id]);
            }
        }
    }

    // Ajouter des écouteurs d'événements
    checkboxes.forEach(checkbox => checkbox.addEventListener("change", updateLayers));
    factionRadios.forEach(radio => radio.addEventListener("change", updateFactionLayers));

    // Initialisation
    updateLayers();
//});



    $.getJSON('/poi', { lang: userLanguage }, function(data) {
        data.forEach(function(marker) {
            var icon = Ryzom.icon(marker.icon);
            var popupContent = marker.title;
            var layerGroup;
            
            if (marker.marker_type === 'guild_agent') {
                layerGroup = layerGroups.GuildLayerGroup;
            }
            else if (marker.marker_type === 'vortex') {
                layerGroup = layerGroups.VortexLayerGroup;
            }
            else if (marker.marker_type === 'temple') {
                layerGroup = layerGroups.TempleLayerGroup;
            }
            else if (marker.marker_type === 'boss_marau') {
                layerGroup = layerGroups.MarauderBossLayerGroup;
            }
            else if (marker.marker_type === 'NH') {
                layerGroup = layerGroups.NhLayerGroup;
            }
            else {
                layerGroup = layerGroups.tpLayerGroup;
            }
            
            var poi = L.marker([marker.lat, marker.lng], {icon: icon, title: marker.title}).addTo(layerGroup);
            poi.bindPopup(popupContent);
        });
    });

    // Récupérer les tribus
    $.getJSON('/tribe', { lang: userLanguage }, function(data) {
    // Définition des traductions

    data.forEach(function(marker) {
        var layerGroup;
        const faction = JSON.parse(marker.faction).fr;

        if (faction === "Kami") {
            layerGroup = layerGroups.kamitribeLayerGroup;
        } else if (faction === "Karavan") {
            layerGroup = layerGroups.karatribeLayerGroup;
        } else if (faction === "Maraudeurs") {
            layerGroup = layerGroups.marautribeLayerGroup;
        }

        var poi = L.marker([marker.lat, marker.lng], {
            icon: Ryzom.icon(marker.icon),
            title: marker.title
        }).addTo(layerGroup);
        poi.addTo(layerGroups.tribeLayerGroup);

        // Vérifier si marker.wiki_link est une chaîne JSON et la parser
        var wikiLinks = marker.wiki_link ? JSON.parse(marker.wiki_link) : null;
        var faction_trads = marker.faction ? JSON.parse(marker.faction) : null;

        // Vérifier si wikiLinks contient la langue demandée, sinon utiliser "default"
        var wikiLink = (wikiLinks && wikiLinks[userLanguage])
            ? wikiLinks[userLanguage]
            : (wikiLinks && wikiLinks['en'])
                ? wikiLinks['en']
                : null;
        var faction_trad = (faction_trads && faction_trads[userLanguage])
            ? faction_trads[userLanguage]
            : (faction_trads && faction_trads['en'])
                ? faction_trads['en']
                : null;

        // Sélectionner les traductions en fonction de userLanguage (fallback en anglais si la langue n'existe pas)
        var factionLabel = translations["Faction"][userLanguage] || translations["Faction"]["en"];
        var wikiLabel = translations["Wiki Link"][userLanguage] || translations["Wiki Link"]["en"];
        var wikiNotAvailable = translations["Wiki Not Available"][userLanguage] || translations["Wiki Not Available"]["en"];

        // Construire le contenu du popup
        var popupContent = '<b>' + marker.title + '</b>' +
                        '<br>' +
                        factionLabel + ": " + faction_trad +
                        '<br>';

        // Ajouter le lien uniquement s'il existe
        if (wikiLink) {
            popupContent += '<a href="' + wikiLink + '" target="_blank">' + wikiLabel + '</a><br>';
        } else {
            popupContent += wikiNotAvailable + '<br>';
        }

        poi.bindPopup(popupContent);
        });
    });

    // Récupérer les bandits
    $.getJSON('/bandit', { lang: userLanguage }, function(data) {
    data.forEach(function(marker) {
        var poi = L.marker([marker.lat, marker.lng], {icon: Ryzom.icon(marker.icon), title: marker.title}).addTo(layerGroups.banditLayerGroup);
        poi.bindPopup(marker.title);
        });
    });

    // Récupérer les ops
    $.getJSON('/outpost', { lang: userLanguage }, function(data) {
    data.forEach(function(marker) {
        var outpost = L.marker([marker.lat, marker.lng], {icon: Ryzom.icon(marker.icon), title: marker.title}).addTo(layerGroups.outpostLayerGroup);
        outpost.bindPopup(marker.title);
        });
    });

    // recupérer les NPC et les ajouter à la recherche
    $.getJSON('/NPC', { lang: userLanguage }, function(data) {
        var npcNames = {};
        data.forEach(function(npc) {
            npcNames[npc.name] = true;
            var poi = L.marker([npc.lat, npc.lng], {icon: Ryzom.icon(npc.icon),
                                                        title: npc.name,
                                                        occupation: npc.title}
                            ).addTo(layerGroups.NpcLayerGroup);

            poi.bindPopup('<b>' + npc.name + '</b>' + '<br>'
                            + '<i>' + npc.title + '</i>' + '<br>'
                            + 'affilié: ' + affiliateTranslation);
            // Vérifie si le champ tribe_affiliate est non nul
            if (npc.tribe_affiliate) {
                var tribeAffiliate = JSON.parse(npc.tribe_affiliate);
                var affiliateTranslation = tribeAffiliate[userLanguage] || tribeAffiliate['en'];
                // Crée un marker pour le NPC dans affiliateLayerGroup
                var affiliatePoi = L.marker([npc.lat, npc.lng], {
                    icon: Ryzom.icon(npc.icon),
                    title: npc.name,
                    occupation: npc.title
                }).addTo(layerGroups.affiliateLayerGroup);

                affiliatePoi.bindPopup('<b>' + npc.name + '</b>' + '<br>'
                                       + '<i>' + npc.title + '</i>' + '<br>'
                                       + 'affilié: ' + affiliateTranslation);
            }
        });

        var uniqueNames = Object.keys(npcNames);

        $("#search").autocomplete({
            appendTo:"#search.uiautocomplete-input",
            source: uniqueNames,
            autocomplete: true,
            minLength: 1,
        });
    });

    var markerLayer = L.featureGroup();
    function handleSearch(event) {
        event.preventDefault();
        const input = document.querySelector("#search");
        const value = input.value; //.toLowerCase();

        // Si la valeur de recherche est vide, supprimer tous les marqueurs de la carte
        if (value === "") {
            markerLayer.clearLayers();
            return;
        }

        map.removeLayer(markerLayer);
        // Créer un nouveau FeatureGroup pour les marqueurs filtrés
        const markers = new L.featureGroup();

        // Requête AJAX pour récupérer les marqueurs correspondant à la recherche
        $.getJSON('/search', {search: value, lang: userLanguage }, function(data) {
        // Ajouter les marqueurs retournés au FeatureGroup
            data.forEach(function(markerData) {
                const marker = L.marker([markerData.lat, markerData.lng], {icon: Ryzom.icon(markerData.icon),
                                                                           title: markerData.title,
                                                                           mp_type: markerData.mp_type,
                                                                           weather: markerData.weather});
                if (markerData.name) {
                    if (markerData.tribe_affiliate) {
                        var Affiliate = JSON.parse(markerData.tribe_affiliate);
                        var NPCaffiliateTranslation = Affiliate[userLanguage] || Affiliate['en'];
                        // Crée un marker pour le NPC dans affiliateLayerGroup
                        popupContent = '<b>' + markerData.name + '</b>'
                                       + '<br>'
                                       + '<i>' + markerData.title + '</i>'
                                       + '<br>' + 'affilié: ' + NPCaffiliateTranslation;
                    }
                    else {
                        popupContent = '<b>' + markerData.name + '</b>'
                                       + '<br>'
                                       + '<i>' + markerData.title + '</i>';
                     }
                }
                else {
                    popupContent = markerData.title;
                }
                marker.bindPopup(popupContent);
                markers.addLayer(marker);
            });

        // Supprimer les anciens marqueurs de la carte et ajouter les nouveaux
        markerLayer = markers.addTo(map);

        // Centrer la carte sur le premier marqueur retourné
        if (data.length > 0) {
            const firstMarker = data[0];
            const latLng = L.latLng(firstMarker.lat, firstMarker.lng);
            map.setView(latLng); // Changer le niveau de zoom selon vos besoins
        }

      });
    }

    // Ajout d'un écouteur d'événement pour les checkbox
    const CheckedlayerGroups = document.querySelectorAll(".layer-group");

    CheckedlayerGroups.forEach(layerGroup => {
        layerGroup.addEventListener("change", updateLayers);
        });;


    // Ajouter un écouteur pour la case à cocher
    var checkbox = document.getElementById('nextseason');
    checkbox.addEventListener('change', function() {
        // Mettre à jour le contrôle en fonction de l'état de la case à cocher
        updateSeasonControl();
    });

    // Ajout d'un écouteur d'événement pour le formulaire
    const form = document.getElementById("searchbar");
    form.addEventListener("submit", handleSearch);

    // Ajout d'un écouteur d'événement pour la touche "Entrée"
    const input = document.querySelector("#search");
    input.addEventListener("keydown", function(event) {
      if (event.key === "Enter") {
        handleSearch(event);
      }
    });

    // ecouteur d'evenements pour la navigation au clavier
    map.keyboard.disable();
    document.addEventListener('keydown', function(event) {
        var delta = 75;
        var key = event.code;
        console.log(event.code)

        if (key === 'F11') {
            if (document.fullscreenElement) {
                document.exitFullscreen();
            } else {
                document.documentElement.requestFullscreen();
            }
        } else if (key === 'NumpadAdd') {
            map.setZoom(map.getZoom() + 1);
        } else if (key === 'NumpadSubtract') {
            map.setZoom(map.getZoom() - 1);
        } else if (key === 'ArrowUp') {
            map.panBy([0, -delta]);
        } else if (key === 'ArrowDown') {
            map.panBy([0, delta]);
        } else if (key === 'ArrowLeft') {
            map.panBy([-delta, 0]);
        } else if (key === 'ArrowRight') {
            map.panBy([delta, 0]);
        }
    });

    function updateMap(data) {
        layerGroups.WeatherLayerGroup.clearLayers();
        weather_now = JSON.parse(data);

        Object.keys(weather_now.continents).forEach(function (continentName) {
            continent = continentName
            cycle = Object.keys(weather_now.continents[continentName])[0]
            condition = weather_now.continents[continentName][cycle].condition
            text = weather_now.continents[continentName][cycle].text

            var icon = iconMapping[condition];
            var position = WeathercontinentPositions[continent];

            var marker = L.marker([position.lat, position.lng], { icon: icon });
            marker.bindPopup(continentName + "<br>Météo: " + condition + "<br>Temps: " + text.substring(2));
            layerGroups.WeatherLayerGroup.addLayer(marker);
        });
    }

    function fetchData() {
        var weatherCheckbox = document.getElementById('WeatherLayerGroup');
        if (weatherCheckbox.checked) {
            $.getJSON('/weather', function(data) {
                updateMap(data);
                });
        };
        updateLayers();
    };

    // Effectuer un appel à l'API toutes les 1.5 minutes
    fetchData();
    setInterval(fetchData, 90000);
});
