#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Importations nécessaires
from flask import Flask, render_template, redirect, url_for, session, jsonify, send_from_directory
from flask import request
import settings
import requests
import time
import urllib
import json
import xmltodict
from peewee import SqliteDatabase, fn # , Model, IntegerField, CharField
from models import Marker  # , Users
import arrow
from flask_babel import Babel, _
import urllib.parse

# Configuration de l'application Flask
app = Flask(__name__)
app.secret_key = settings.secret_key
# Configuration de Babel
app.config['BABEL_DEFAULT_LOCALE'] = 'en'  # Langue par défaut
app.config['LANGUAGES'] = ['en', 'fr', 'de', 'es', 'ru']  # Langues prises en charge


def get_locale():
    return request.accept_languages.best_match(app.config['LANGUAGES'])

babel = Babel(app, locale_selector=get_locale)

# Configuration de la base de données SQLite
DATABASE = 'markers.db'
db = SqliteDatabase(DATABASE)

# API Ryzom
ryzom_api = 'https://api.ryzom.com/'
time_api = ryzom_api + 'time.php?format=xml'
weather_api = ryzom_api + 'weather.php?cycles=0'


def check_lang(request_to_check):
    #supported_languages = ['en', 'fr', 'de', 'es', 'ru']
    lang = request_to_check.accept_languages.best_match(app.config['LANGUAGES'])

    if lang:
        lang = lang.split('-')[0]
    else:
        lang = 'en'
    return lang

# Route de la page d'accueil
@app.route('/')
def home():
    return redirect(url_for('mymap'))


# Route de la page de la carte Leaflet
@app.route('/map')
def mymap():
    lang = check_lang(request)
    print(lang)

    try:
        user_name = session['user_info']['name']
    except KeyError:
        user_name = 'John Doe'

    json_data = json.dumps({'lang': get_locale(),
                            'name': user_name,
                            'disconnect': _('Déconnecter '),
                            'search': _('Rechercher...'),
                            'condition': _('Condition'),
                            'weather': _('Temps')})
    
    return render_template('index.html', user_info=session.get('user_info'), json_data=json_data)


@app.route('/next_season')
def next_season():
    lang = check_lang(request)
    now = int(time.time())
    response = requests.get(time_api)
    jsondata = xmltodict.parse(response.text)

    h = int(jsondata['shard_time']['time_of_day'])
    s = (jsondata['shard_time']['season'])

    e = 388800 - ((int(jsondata['shard_time']['day_of_season']) * 4320) + (h * 180))
    dd = 0
    hh = 0
    mm = 0
    ss = (now + e) - now

    while ss >= 86400:
        dd += 1
        ss -= 86400

    while ss >= 3600:
        hh += 1
        ss -= 3600

    while ss >= 60:
        mm += 1
        ss -= 60

    nextseason = [_("l'été"), _("l'automne"), _("l'hiver"), _('le printemps')][int(s)]
    nexttime = arrow.utcnow().shift(seconds=+ e)
    nexttime = nexttime.humanize(granularity=['day', 'hour', 'minute'], locale=get_locale())
    next_date = arrow.now().shift(seconds=+ e).format("DD MMM, HH[h]mm", locale=get_locale())

    return jsonify({"message": _('%s débute<br />%s<br />ce %s') % (nextseason, nexttime, next_date)})


@app.route('/vortex_path', methods=['GET'])
def vortex_path():
    with open('vortex_path.json', 'r') as f:
        vortex_path_file = json.load(f)
    return jsonify(vortex_path_file)


@app.route('/weather')
def weather():
    response = requests.get(weather_api)
    return jsonify(response.text)


# Route pour récupérer les enregistrements filtrés sur le champ marker_type de la table markers
@app.route('/<marker_type>')
def get_markers(marker_type):
    lang = request.args.get('lang', app.config['BABEL_DEFAULT_LOCALE'])
    #lang = check_lang(request)
    
    if marker_type == 'poi':
        markers = Marker.select().where(Marker.marker_type.in_(['guild_agent',
                                                                'vortex',
                                                                'TP',
                                                                'temple',
                                                                'boss_marau',
                                                                'NH']))
    else:
        markers = Marker.select().where(Marker.marker_type == marker_type)

    results = list()
    # results = [{'marker_id': m.marker_id,
    #             'marker_type': m.marker_type,
    #             'lat': m.lat,
    #             'lng': m.lng,
    #             'mp_type': m.mp_type,
    #             'icon': m.icon,
    #             'name': m.name,
    #             'quality': m.quality,
    #             'title': json.loads(m.title).get(lang, m.title),
    #             'season': m.season,
    #             'weather': m.weather} for m in markers]
    for m in markers:
        try:
            title = json.loads(m.title).get(lang, m.title)
        except (json.JSONDecodeError, TypeError):
            title = m.title
        try:
            name = json.loads(m.name).get(lang, m.name)
        except (json.JSONDecodeError, TypeError):
            name = m.name

        results.append({
            'marker_id': m.marker_id,
            'marker_type': m.marker_type,
            'lat': m.lat,
            'lng': m.lng,
            'mp_type': m.mp_type,
            'icon': m.icon,
            'name': name,
            'quality': m.quality,
            'title': title,
            'season': m.season,
            'weather': m.weather,
            'wiki_link': m.wiki_link,
            'tribe_affiliate': m.tribe_affiliate,
            'faction': m.faction,
        })

    return jsonify(results)

# Route pour la recherche de marqueurs
@app.route('/search')
def search_markers():
    query = urllib.parse.unquote(request.args.get('search'))
    print(query)
    lang = request.args.get('lang', app.config['BABEL_DEFAULT_LOCALE'])

    if query:
        # Recherche des marqueurs correspondant à la requête
        markers = Marker.select().where(
            (Marker.title.contains(query)) |
            (Marker.name.contains(query)) |
            (Marker.tribe_affiliate.contains(query))
        )
        print(markers)
        print(list(markers))

        results = list()
        for m in markers:
            try:
                title = json.loads(m.title).get(lang, m.title)
            except (json.JSONDecodeError, TypeError):
                title = m.title
            try:
                name = json.loads(m.name).get(lang, m.name)
            except (json.JSONDecodeError, TypeError):
                name = m.name

            results.append({
                'marker_id': m.marker_id,
                'marker_type': m.marker_type,
                'lat': m.lat,
                'lng': m.lng,
                'mp_type': m.mp_type,
                'icon': m.icon,
                'name': name,
                'quality': m.quality,
                'title': title,
                'season': m.season,
                'weather': m.weather,
                'wiki_link': m.wiki_link,
                'tribe_affiliate': m.tribe_affiliate,
                'faction': m.faction
            })

        # Renvoi des résultats au format JSON
        print(results)
        return jsonify(results)
    else:
        # Renvoi d'une erreur si la requête ne contient pas de paramètre "query"
        return jsonify({'error': 'Missing query parameter'}), 400


@app.route('/maps/<path:path>')
def send_report(path):
    return send_from_directory('maps', path)


# Point d'entrée de l'application
if __name__ == '__main__':
    app.run(debug=True)
